const bill = document.getElementById("bill"),
    billMessage = document.getElementById("billMessage")

bill.style.display = "none"

var config = {
    apiKey: "AIzaSyBkJpmB96ppEZZqzkU6i-i5P-6JxL_NUcA",
    authDomain: "parking-faa7f.firebaseapp.com",
    databaseURL: "https://parking-faa7f.firebaseio.com",
    projectId: "parking-faa7f",
    storageBucket: "",
    messagingSenderId: "923992150418"
};
firebase.initializeApp(config);

const db = firebase.database(),
    carsRef = db.ref().child("cars")


const provider = new firebase.auth.GoogleAuthProvider(),
    auth = firebase.auth(),
    user = firebase.auth.currentUser,
    loginMessage = document.getElementById("login-message"),
    authMessage = document.getElementById("session-message")

firebase.auth().onAuthStateChanged( user => {
    // console.log( user )
    if (user) {
        console.log('Usuario Autenticado')
        authMessage.innerHTML = `
          <p>Si ves este contenido, es por que estás <b>logueado :)</b></p>
          <button id="logout">Salir</button>
        `
    } else {
        console.log('Usuario NO Autenticado')
        authMessage.innerHTML = '<p>Sólo <b>administradores</b> pueden hacer cambios.</p>'
    }
} )


document.addEventListener("contextmenu", function (e) {
    e.preventDefault()
    if (e.target.matches(".box.full")) {
        alert("¿seguro que desea eliminar esto?")
    }
}, false)

const carForm = document.getElementById("createOrUpdate"),
    carId = document.getElementById("carId"),
    carPlate = document.getElementById("carPlate"),
    carState = document.getElementById("carState"),
    submit = document.getElementById("submit")

function boxTemplate ( {carPlate, state, createdTime, typeOfVehicle} ) {
    return `
        <div class="box ${state} ${carPlate} ${typeOfVehicle}">
        </div>
        <div style="background-color: rgba(40, 40, 40, .3); text-align: center; border: 1px solid white; padding: 5px;">
            <span>${carPlate}</span>
            <br>
            <span>${createdTime.hours}:${createdTime.minutes}</span>
        </div>
        <button id="quit" class="button button--delete">QUITAR</button>
    `
}

// CREATE
submit.addEventListener("click", function (e) {

    e.preventDefault()

    //obtener valores del objeto Date
    const current = getCurrentTime(),
        createdTime = { hours: current.hours, minutes: current.minutes }


    typeOfVehicle = document.querySelector('input[name="type"]:checked')

    let id = carId.value || carsRef.push().key,
        carsData = {
            carPlate: carPlate.value,
            state: carState.value,
            createdTime: createdTime,
            typeOfVehicle: typeOfVehicle.value
        },
        updateData = {}

    updateData[`/${id}`] = carsData
    carsRef.update( updateData )

    carId.value = ""
    carForm.reset()
    
})

// READ
carsRef.on("child_added", data => {
    let li = document.createElement("li")
    li.id = data.key
    //box.classList.add(`${data.val().carPlate}`)
    li.innerHTML = boxTemplate(data.val())    
    boxes.appendChild(li)
})

carsRef.on('child_changed', data => {
    let affectedNode = document.getElementById(data.key)
        affectedNode.innerHTML = boxTemplate(data.val())
})

carsRef.on("child_removed", data => {
    let affectedNode = document.getElementById(data.key)
    boxes.removeChild( affectedNode )
})


document.addEventListener("click", function (e) {
    let affectedNode = e.target.parentNode,
        globalId = ""

    if ( e.target.matches("#login") ) {
        e.preventDefault()

        firebase.auth().signInWithPopup( provider )

            .then( result => {

                // The signed-in user info.
                var user = result.user;
                loginMessage.innerHTML = `<p class="ok"><b>${result.user.email}</b> ha iniciado sesión con Google.</p><img height="80px" width="80px" style="border-radius: 50%" src="${result.user.photoURL}">`
            } )
            .catch( err => {
                loginMessage.innerHTML = `<p class="error">Error de Autenticación con Google ${err.code}: <b>${err.message}</b>.</p>`
            } )
    }

    // if ( e.target.matches(".box.empty") ) {
	// 	e.target.classList.remove("empty")
	// 	e.target.classList.add("full")
	// 	const startTime = new Date()
	// 	console.log("inicio: ", startTime.getHours(), startTime.getMinutes(), startTime.getSeconds())
	// } else {
	// 	if ( e.target.matches(".box.full") ) {
	// 		e.target.classList.remove("full")
	// 		e.target.classList.add("empty")
	// 		const endTime = new Date()
	// 		console.log("fin: ", endTime.getHours(), endTime.getMinutes(), endTime.getSeconds())
	// 	}
    // }

    // UPDATE
    if ( e.target.matches(".box") ) {
        // UPDATE
        carPlate.value = e.target.getAttribute("class").split(" ")[2]
        carId.value = affectedNode.id
    }

    if ( e.target.matches(".delete") ) {
        e.preventDefault()
        let id = carId.value
        db.ref(`cars/${id}`).remove()
    }

    if ( e.target.matches("#quit") ) {
        bill.style.display = "block"
        setIdFromButton( affectedNode )
        billMessage.innerHTML = getBillMessage( affectedNode )
    }

    if ( e.target.matches("#buttonOk") ) {
        let id = getIdFromButton()
        bill.style.display = "none"
        db.ref(`cars/${id}`).remove()
    }

    if ( e.target.matches("#buttonWait") ) {
        bill.style.display = "none"
    }


    // LOGOUT
    if ( e.target.matches("#logout") ) {
        auth.signOut()
            .then( () => {
                console.log("sesion terminada")
                authMessage.innerHTML = "<p>Sesion terminada</p>"
                loginMessage.innerHTML = ""
            } )
            .catch( err => {
                console.log( err )
            } )
    }
})

// functiones para el calculo de tiempos
const getCurrentTime = () => {
    const current = new Date(),
        hours = current.getHours(),
        minutes = current.getMinutes()

    return {
        minutes,
        hours
    }
}

const getParkingTime = id => {
    const now = getCurrentTime(),
        currentHours = now.hours,
        currentMinutes = now.minutes

    carsRef.on("child_added", data => {
        // console.log( data.val().createdTime )

        if ( data.key == id ) {
            // console.log( data.val().typeOfVehicle )
            hours =  currentHours - data.val().createdTime.hours
            minutes = currentMinutes - data.val().createdTime.minutes
            typeOfVehicle =  data.val().typeOfVehicle
        }
    })

    return {
        hours: hours,
        minutes: minutes,
        typeOfVehicle: typeOfVehicle
    }
}

const getPrice = id => {
    const time = getParkingTime(id),
        costPerHourCar = 5,
        costPerHourMotocycle = 4

    let toPaid = 0,
        charge = 0
    
    if ( time.typeOfVehicle == "car" ) {
        if (time.minutes >= 5) {
            charge = 5
        }
        toPaid = charge + costPerHourCar * time.hours
    } else {
        if ( time.typeOfVehicle == "motocycle" ) {
            if (time.minutes >= 5) {
                charge = 4
            }
            toPaid = charge + costPerHourMotocycle * time.hours
        }   
    }

    const getBillMessage = ( node ) => {
        return `El precio a pagar es: ${getPrice( node.id ).toPaid}, horas en parkeo: ${getPrice( node.id ).hours}, cargos aplicados: ${getPrice( node.id ).charge}}`
    }

    return {
        toPaid,
        hours,
        minutes,
        typeOfVehicle: time.typeOfVehicle,
        charge
    }
}

const getBillMessage = node => {
    return `horas en parkeo: ${getPrice( node.id ).hours}<br>Cargos aplicados: ${getPrice( node.id ).charge}<br><strong>El precio a pagar es: ${getPrice( node.id ).toPaid}</strong>`
}

const setIdFromButton = parentNode => {
   buttonId = parentNode.id
}

const getIdFromButton = () => buttonId