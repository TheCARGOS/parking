
var config = {
    apiKey: "AIzaSyBkJpmB96ppEZZqzkU6i-i5P-6JxL_NUcA",
    authDomain: "parking-faa7f.firebaseapp.com",
    databaseURL: "https://parking-faa7f.firebaseio.com",
    projectId: "parking-faa7f",
    storageBucket: "",
    messagingSenderId: "923992150418"
};
firebase.initializeApp(config);

const db = firebase.database(),
    carsRef = db.ref().child("cars")


const provider = new firebase.auth.GoogleAuthProvider(),
    auth = firebase.auth(),
    user = firebase.auth.currentUser,
    loginMessage = document.getElementById("login-message"),
    authMessage = document.getElementById("session-message")

firebase.auth().onAuthStateChanged( user => {
    console.log( user )
    if (user) {
        console.log('Usuario Autenticado')
        authMessage.innerHTML = `
          <p>Si ves este contenido, es por que estás <b>logueado :)</b></p>
          <button id="logout">Salir</button>
        `
    } else {
        console.log('Usuario NO Autenticado')
        authMessage.innerHTML = '<p>Sólo <b>administradores</b> pueden hacer cambios.</p>'
    }
} )



document.addEventListener("contextmenu", function (e) {
    e.preventDefault()
    if (e.target.matches(".box.full")) {
        alert("¿seguro que desea eliminar esto?")
    }
}, false)

const carForm = document.forms[0],
    carId = document.getElementById("carId"),
    carPlate = document.getElementById("carPlate"),
    carState = document.getElementById("carState")
    submit = document.getElementById("submit")

function createTemplate ( {carPlate, state, createdTime} ) {
    return `
        <div class="box ${state} ${carPlate}">
        </div>
        <span>${carPlate}</span>
        <br>
        <span>${createdTime.hours}</span>:<span>${createdTime.minutes}</span>
    `
}

function updateTemplate ({carPlate, state, createdTime}) {
    return `
        <div class="box ${state} ${carPlate}">
        </div>
        <span>${carPlate}</span>
        <span>${createdTime.hours}</span>:<span>${createdTime.minutes}</span>        
        <br>
    `
}

// CREATE
submit.addEventListener("click", function (e) {

    e.preventDefault()

    const createdTime = {
        hours: new Date().getHours(),
        minutes: new Date().getMinutes()
    }

    let id = carId.value || carsRef.push().key,
        carsData = {
            carPlate: carPlate.value,
            state: carState.value,
            createdTime: createdTime
        },
        updateData = {}

    updateData[`/${id}`] = carsData
    carsRef.update( updateData )

    carId.value = ""
    carForm.reset()
    
})

// READ
carsRef.on("child_added", data => {
    let li = document.createElement("li")
    li.id = data.key
    //box.classList.add(`${data.val().carPlate}`)
    li.innerHTML = createTemplate(data.val())    
    boxes.appendChild(li)
})

carsRef.on('child_changed', data => {
    let affectedNode = document.getElementById(data.key)
        affectedNode.innerHTML = updateTemplate(data.val())
})

carsRef.on("child_removed", data => {
    let affectedNode = document.getElementById(data.key)
    boxes.removeChild( affectedNode )
})


document.addEventListener("click", function (e) {
    e.preventDefault()
    let affectedNode = e.target.parentNode
    if ( e.target.matches("#login") ) {
        firebase.auth().signInWithPopup( provider )

            .then( result => {
                console.log(result.credential)

                // The signed-in user info.
                var user = result.user;
                loginMessage.innerHTML = `<p class="ok"><b>${result.user.email}</b> ha iniciado sesión con Google.</p><img height="80px" width="80px" style="border-radius: 50%" src="${result.user.photoURL}">`
            } )
            .catch( err => {
                loginMessage.innerHTML = `<p class="error">Error de Autenticación con Google ${err.code}: <b>${err.message}</b>.</p>`
            } )
    }

    // if ( e.target.matches(".box.empty") ) {
	// 	e.target.classList.remove("empty")
	// 	e.target.classList.add("full")
	// 	const startTime = new Date()
	// 	console.log("inicio: ", startTime.getHours(), startTime.getMinutes(), startTime.getSeconds())
	// } else {
	// 	if ( e.target.matches(".box.full") ) {
	// 		e.target.classList.remove("full")
	// 		e.target.classList.add("empty")
	// 		const endTime = new Date()
	// 		console.log("fin: ", endTime.getHours(), endTime.getMinutes(), endTime.getSeconds())
	// 	}
    // }

    // UPDATE
    if ( e.target.matches(".box") ) {
        // UPDATE
        carPlate.value = e.target.getAttribute("class").split(" ")[2]
        carId.value = affectedNode.id
    }

    if ( e.target.matches(".delete") ) {
        let id = carId.value
        db.ref(`cars/${id}`).remove()
    }


    // LOGOUT
    if ( e.target.matches("#logout") ) {
        auth.signOut()
            .then( () => {
                console.log("sesion terminada")
                authMessage.innerHTML = "<p>Sesion terminada</p>"
                loginMessage.innerHTML = ""
            } )
            .catch( err => {
                console.log( err )
            } )
    }
})