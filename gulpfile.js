const gulp = require("gulp"),
    sass = require("gulp-sass")

gulp
    .task("sass", function () {
        gulp
            .src("./scss/*.scss")
            .pipe( sass() )
            .pipe( gulp.dest("./public/css") )
    })
    
gulp
    .task("default", function () {
        gulp.watch("./scss/**/*.scss", "sass", function () {
            console.log("is this work?")
        })
    })