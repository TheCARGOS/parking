const express = require("express"),
    app = express(),
    routes = require("./routes/index")

app
    .set("view engine", "pug")

    .use( routes )
    .use( express.json() )
    .use(express.static("public"))
    .use( express.urlencoded({extended: true}) )

    .listen(7373, () => {
        console.log("Server is running at 8080 port.")
    })